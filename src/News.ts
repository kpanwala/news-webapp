export class News{

    source:string;
    author:any;
    title:string;
    description:string;
    url:string;
    urlToImage:string;
    publishedAt:string;
    content:string;

    constructor(source:any,author:string,title:string,description:string,url:string,urlToImage:string,publishedAt:string,content:string){
        this.author=author;
        this.source=source;
        this.title=title;
        this.description=description;
        this.url=url;
        this.urlToImage=urlToImage;
        this.publishedAt=publishedAt;
        this.content=content;
    }
}