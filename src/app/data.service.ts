import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {News} from '../News';


@Injectable({
  providedIn: 'root'
})
export class DataService {

  private REST_API_SERVER = "https://newsapi.org/v2/top-headlines?country=in&apiKey=6894937725e6436196b65cb4a5c49c50";

  constructor(private httpClient: HttpClient) { }

  public getNews(){
    return this.httpClient.get(this.REST_API_SERVER);
  }
}