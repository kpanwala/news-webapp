import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'single-card',
  templateUrl: './single-card.component.html',
  styleUrls: ['./single-card.component.css']
})
export class SingleCardComponent implements OnInit {

  @Input() hero:any
  constructor() { }

  ngOnInit(): void {
  }

}
