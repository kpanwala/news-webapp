import { Component } from '@angular/core';
import { News } from 'src/News';
import { DataService } from './data.service'; 

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers : [DataService]  
})
export class AppComponent {
  title = 'news';

  newsArr=[];

  newsArrIdx = ['source','author','title','description','url','urlToImage','publishedAt','content'];

  constructor(private dataService: DataService) { }

  ngOnInit() {

    this.dataService.getNews().subscribe(
      (data:any) => {
      console.log(data.articles);
      this.newsArr = data.articles;
    })  
  }
}
